package gts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.PixelmonMethods;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.comm.packetHandlers.RequestUpdatedPokemonList;
import com.pixelmonmod.pixelmon.comm.packetHandlers.clientStorage.Remove;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.database.DatabaseMoves;
import com.pixelmonmod.pixelmon.database.Move;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Gender;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Level;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import com.pixelmonmod.pixelmon.enums.EnumGrowth;
import com.pixelmonmod.pixelmon.enums.EnumNature;
import com.pixelmonmod.pixelmon.enums.EnumPokeballs;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerComputerStorage;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import gts.util.UUIDHelper;
import gts.util.Utilities;
import gts.util.db.Database;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.storage.SaveHandler;
import net.minecraft.world.storage.SaveHandlerMP;

public class GTSCommand extends CommandBase {
    Base b = Base.instance;
    Database db = b.db;

    public GTSCommand() {
    }

    @Override
    public String getCommandName() {
        return "gts";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/gts help";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts <add, search, cancel, trade, check, help, list>");
            return;
        }

        try {
            EntityPlayerMP player = getPlayer(sender, sender.getCommandSenderName());
            int position = 0;
            PlayerStorage storage = null;

            if (args[0].equalsIgnoreCase("add")) {
                if (args.length < 3) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts add <slot> <wanted pokemon> <s?>");
                    return;
                }

                if (hasActiveTrade(player)) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.RED + "You already have an active trade! Cancel your old one first!");
                    return;
                }
                position = Integer.parseInt(args[1]) - 1;
                try {
                    storage = PixelmonStorage.PokeballManager.getPlayerStorage(player);
                    storage.recallAllPokemon();
                } catch (PlayerNotLoadedException e) {
                    sender.addChatMessage(new ChatComponentText("Error in getting player storage"));
                    return;
                }

                int num = storage.count();
                if (num <= 1) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.RED + "You need more than one pokemon in your party to use GTS!");
                    return;
                }

                NBTTagCompound[] nbt = storage.getList();
                if (nbt[position] == null) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.RED + "There is no Pokemon in that slot!");
                    return;
                }

                NBTTagCompound pokenbt = nbt[position];
                NBTTagCompound n1 = null;
                storage.changePokemonAndAssignID(position, n1);
                EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(pokenbt, sender.getEntityWorld());
                String w = args[2];

                if (!EnumPokemon.hasPokemonAnyCase(w)) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.RED + "No pokemon exists by that name!");
                    return;
                }

                EntityPixelmon wp = (EntityPixelmon) PixelmonEntityList.createEntityByName(w, sender.getEntityWorld());
                for (int i = 2; i < args.length; i++) {
                    sh = args[i].equalsIgnoreCase("s");
                }
                addToDB(player, pokemon, wp, sh);
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You've added " + pokemon.getName() + " to the GTS for " + (sh ? "shiny " : "") + wp.getName());
            } else if (args[0].equalsIgnoreCase("search")) {
                if (args.length < 2) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts search <pokemon>");
                    return;
                }

                String searchp = args[1];

                if (!EnumPokemon.hasPokemonAnyCase(searchp)) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.RED + "Pokemon does not exist by that name!");
                    return;
                }

                String ps = EnumPokemon.getFromNameAnyCase(searchp).name();
                db.connect();
                int sAm = searchResultsAm(ps);
                for (int i = 1; i <= sAm; i++) {
                    String sellingPlayer = (String) db.get(Tables.GTS_T, "pokemon", "Seller", ps);
                    String spName = (String) db.get(Tables.GTS_T, "pokemon", "SellerName", ps);
                    int id = (Integer) db.get(Tables.GTS_T, "SellerName", "id", spName);
                    String pShiny = (String) db.get(Tables.GTS_T, "SellerName", "Shiny", spName);
                    String wPoke = (String) db.get(Tables.GTS_T, "SellerName", "Wanted", spName);
                    String wShiny = (String) db.get(Tables.GTS_T, "SellerName", "WantShiny", spName);
                    if (pShiny.equalsIgnoreCase("true") && wShiny.equalsIgnoreCase("true")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + id + ". " + spName + " is trading a shiny " + ps + " for a shiny " + wPoke);
                    } else if (pShiny.equals("false") && wShiny.equals("true")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + id + ". " + spName + " is trading a " + ps + " for a shiny " + wPoke);
                    } else if (pShiny.equals("true") && wShiny.equals("false")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + id + ". " + spName + " is trading a shiny " + ps + " for a " + wPoke);
                    } else {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + id + ". " + spName + " is trading a " + ps + " for a " + wPoke);
                    }
                }
            } else if (args[0].equalsIgnoreCase("cancel")) {
                if (hasActiveTrade(player)) {
                    seller = UUIDHelper.getOfflineUUID(player.getDisplayName());
                    db.connect();
                    int id = (Integer) db.get(Tables.GTS_T, "Seller", "id", seller);
                    String pixel = (String) db.get(Tables.GTS_T, "Seller", "pokemon", seller);
                    givePoke(player, id, pixel);
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You have canceled your trade and retrieved your ");
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + pixel + ".");
                } else {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You do not have an active trade, use /gts add to make ");
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "one!");
                }
            } else if (args[0].equalsIgnoreCase("trade")) {
                if (args.length < 4) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts trade <wanted pokemon> <ID # of trade> ");
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + " <slot of pokemon to trade>");
                    return;
                }

                PlayerStorage storage2 = null;
                db.connect();
                String wpix = EnumPokemon.getFromNameAnyCase(args[1]).name();
                int id = Integer.parseInt(args[2]);
                position = Integer.parseInt(args[3]) - 1;
                String tfor = (String) db.get(Tables.GTS_T, "id", "pokemon", id);
                String sellguyS = (String) db.get(Tables.GTS_T, "id", "SellerName", id);
                EntityPlayerMP sellguy = MinecraftServer.getServer().getConfigurationManager().func_152612_a(sellguyS);

                if (sellguy != null) {
                    sellguy = getPlayer(player, sellguyS);
                    int pid = (Integer) db.get(Tables.GTS_T, "pokemon", "id", wpix);
                    if (tfor.equalsIgnoreCase(wpix) && pid == id) {
                        String sellwants = (String) db.get(Tables.GTS_T, "id", "Wanted", id);
                        String wantshine = (String) db.get(Tables.GTS_T, "id", "WantShiny", id);

                        try {
                            storage = PixelmonStorage.PokeballManager.getPlayerStorage(player);
                            storage2 = PixelmonStorage.PokeballManager.getPlayerStorage(sellguy);
                            storage.recallAllPokemon();
                            storage2.recallAllPokemon();
                        } catch (PlayerNotLoadedException e) {
                            sender.addChatMessage(new ChatComponentText("Error in getting player storage"));
                            return;
                        }

                        int num = storage.count();
                        if (num <= 1) {
                            NBTTagCompound[] nbt = storage.getList();
                            if (nbt[position] == null) {
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.RED + "There is no pokemon in that slot!");
                                return;
                            }
                            NBTTagCompound pokenbt = nbt[position];
                            NBTTagCompound n1 = null;
                            EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(pokenbt, player.getEntityWorld());

                            if (wantshine.equals("true") && pokenbt.getInteger("IsShiny") == 1 && sellwants.equalsIgnoreCase(pokemon.getName())) {
                                storage.changePokemonAndAssignID(position, n1);
                                storage2.changePokemonAndAssignID(position, pokenbt);
                                givePoke(player, id, wpix);
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                                Utilities.sendMessageToPlayer(sellguy, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                            } else if (wantshine.equals("false") && pokenbt.getInteger("IsShiny") == 0 && sellwants.equalsIgnoreCase(pokemon.getName())) {
                                storage.changePokemonAndAssignID(position, n1);
                                storage2.changePokemonAndAssignID(position, pokenbt);
                                givePoke(player, id, wpix);
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                                Utilities.sendMessageToPlayer(sellguy, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                            } else {
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] That is not what the trader is looking for!");
                            }
                        }
                    }
                } else {
                    int pid = (Integer) db.get(Tables.GTS_T, "pokemon", "id", wpix);
                    if (tfor.equalsIgnoreCase(wpix) && pid == id) {
                        String sellwants = (String) db.get(Tables.GTS_T, "id", "Wanted", id);
                        String wantshine = (String) db.get(Tables.GTS_T, "id", "WantShiny", id);

                        try {
                            storage = PixelmonStorage.PokeballManager.getPlayerStorage(player);
                            storage.recallAllPokemon();
                        } catch (PlayerNotLoadedException e) {
                            sender.addChatMessage(new ChatComponentText("Error in getting player storage"));
                            return;
                        }

                        int num = storage.count();
                        if (num > 1) {
                            NBTTagCompound[] nbt = storage.getList();
                            if (nbt[position] == null) {
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.RED + "There is no pokemon in that slot!");
                                return;
                            }

                            NBTTagCompound pokenbt = nbt[position];
                            NBTTagCompound n1 = null;
                            EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(pokenbt, player.getEntityWorld());

                            if (wantshine.equals("true") && pokenbt.getInteger("IsShiny") == 1 && sellwants.equalsIgnoreCase(pokemon.getName())) {
                                offAddToDB(sellguyS, pokemon);
                                storage.changePokemonAndAssignID(position, n1);
                                givePoke(player, id, wpix);
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                            } else if (wantshine.equals("false") && pokenbt.getInteger("IsShiny") == 0 && sellwants.equalsIgnoreCase(pokemon.getName())) {
                                offAddToDB(sellguyS, pokemon);
                                storage.changePokemonAndAssignID(position, n1);
                                givePoke(player, id, wpix);
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
                            } else {
                                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] That is not what the trader is looking for!");
                            }
                        }
                    }
                }
            } else if (args[0].equalsIgnoreCase("check")) {
                if (!hasActiveTrade(player)) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You do not have an active trade, start one with /gts add!");
                    return;
                }

                seller = UUIDHelper.getOfflineUUID(player.getDisplayName());
                db.connect();
                int id = (Integer) db.get(Tables.GTS_T, "Seller", "id", seller);
                String pixel = (String) db.get(Tables.GTS_T, "Seller", "pokemon", seller);
                String want = (String) db.get(Tables.GTS_T, "Seller", "Wanted", seller);
                String shiny = (String) db.get(Tables.GTS_T, "Seller", "Shiny", seller);
                String wshiny = (String) db.get(Tables.GTS_T, "Seller", "Shiny", seller);

                boolean shi = shiny.equals("true");
                boolean wsh = wshiny.equals("true");

                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + "You are trading a " + (shi ? "shiny " : "") + pixel + " for a " + (wsh ? "shiny " : "") + want + ".");
            } else if (args[0].equalsIgnoreCase("help")) {
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts add - Add a pokemon to the GTS for one of your ");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "choice");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts search - Search for a pokemon in the GTS by name");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts cancel - Cancel any trade from the GTS");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts trade - Trade for the specified pokemon");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts check - Display your current trade in the GTS");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts help - Shows this menu");
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts list - Show the entire GTS trade list");
            } else if (args[0].equalsIgnoreCase("list")) {
                if (args.length > 1) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts list");
                    return;
                }

                int trades = tradesInDB();

                if (trades < 1) {
                    Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] There are no trades in the GTS currently!");
                    return;
                }

                db.connect();
                Connection con = db.getConnection();
                PreparedStatement s = con.prepareStatement("SELECT * FROM gts WHERE id IS NOT NULL ORDER BY id");
                ResultSet rs = s.executeQuery();
                while (rs.next()) {
                    int i = rs.getInt("id");
                    String sellingPlayer = (String) db.get(Tables.GTS_T, "id", "Seller", i);
                    String spName = (String) db.get(Tables.GTS_T, "id", "SellerName", i);
                    String pixel = (String) db.get(Tables.GTS_T, "id", "pokemon", i);
                    String pShiny = (String) db.get(Tables.GTS_T, "SellerName", "Shiny", spName);
                    String wPoke = (String) db.get(Tables.GTS_T, "SellerName", "Wanted", spName);
                    String wShiny = (String) db.get(Tables.GTS_T, "SellerName", "WantShiny", spName);
                    if (pShiny.equalsIgnoreCase("true") && wShiny.equalsIgnoreCase("true")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + i + ". " + spName + " is trading a shiny " + pixel + " for a shiny " + wPoke);
                    } else if (pShiny.equals("false") && wShiny.equals("true")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + i + ". " + spName + " is trading a " + pixel + " for a shiny " + wPoke);
                    } else if (pShiny.equals("true") && wShiny.equals("false")) {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + i + ". " + spName + " is trading a shiny " + pixel + " for a " + wPoke);
                    } else {
                        Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] " + EnumChatFormatting.GOLD + i + ". " + spName + " is trading a " + pixel + " for a " + wPoke);
                    }
                }
            } else {
                Utilities.sendMessageToCommandSender(sender, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] /gts <add, search, cancel, trade, check, help, list>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int searchResultsAm(String searchp) throws SQLException {
        int results = 0;
        db.connect();
        Connection con = db.getConnection();
        PreparedStatement ps = con.prepareStatement("SELECT * FROM gts WHERE pokemon IS NOT NULL ORDER BY pokemon");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            if (rs.getString("pokemon").equalsIgnoreCase(searchp)) {
                results++;
            }
        }
        return results;
    }

    public int tradesInDB() throws SQLException {
        db.connect();
        Connection con = db.getConnection();
        PreparedStatement s = con.prepareStatement("SELECT * FROM gts WHERE id IS NOT NULL ORDER BY id");
        ResultSet result = s.executeQuery();
        int total = 0;
        while (result.next()) {
            if (result.getInt("id") >= 0) {
                total++;
            }
        }
        return total;
    }

    public boolean hasActiveTrade(EntityPlayerMP player) throws SQLException {
        seller = UUIDHelper.getOfflineUUID(player.getDisplayName());
        db.connect();
        return db.contains(Tables.GTS_T, "Seller", seller);
    }

    public void givePoke(EntityPlayerMP player, int id, String pokemon) throws SQLException {
        final int tid = id;
        final String pokemonS = pokemon;
        final EntityPlayerMP entityPlayer = getPlayer(player, player.getCommandSenderName());
        final EntityPixelmon p = (EntityPixelmon) PixelmonEntityList.createEntityByName(pokemonS, entityPlayer.worldObj);

        // This nuisance will be fixed
        new Thread(new Runnable() {
            public void run() {
                try {
                    db.connect();
                    if (db.contains(Tables.GTS_T, "id", tid)) {
                        NBTTagCompound nbt = new NBTTagCompound();
                        nature = (String) db.get(Tables.GTS_T, "id", "Nature", tid);
                        growth = (String) db.get(Tables.GTS_T, "id", "Growth", tid);
                        gender = (String) db.get(Tables.GTS_T, "id", "Gender", tid);
                        shine = (String) db.get(Tables.GTS_T, "id", "Shiny", tid);
                        m1 = (String) db.get(Tables.GTS_T, "id", "Move1", tid);
                        m2 = (String) db.get(Tables.GTS_T, "id", "Move2", tid);
                        m3 = (String) db.get(Tables.GTS_T, "id", "Move3", tid);
                        m4 = (String) db.get(Tables.GTS_T, "id", "Move4", tid);
                        ball = (String) db.get(Tables.GTS_T, "id", "Ball", tid);
                        p.setNature(EnumNature.natureFromString(nature));
                        p.setGrowth(EnumGrowth.growthFromString(growth));
                        p.gender = Gender.valueOf(gender);
                        p.caughtBall = EnumPokeballs.valueOf(ball);
                        if (m2 != null && m3 != null && m4 != null) {
                            p.loadMoveset(m1, m2, m3, m4);
                        } else if (m2 != null && m3 != null) {
                            p.loadMoveset(m1, m2, m3);
                        } else if (m2 != null) {
                            p.loadMoveset(m1, m2);
                        } else {
                            p.loadMoveset(m1);
                        }
                        OT = (String) db.get(Tables.GTS_T, "id", "OTrainer", tid);
                        lvl = (Integer) db.get(Tables.GTS_T, "id", "Level", tid);
                        hp = (Integer) db.get(Tables.GTS_T, "id", "HP", tid);
                        a = (Integer) db.get(Tables.GTS_T, "id", "Attack", tid);
                        d = (Integer) db.get(Tables.GTS_T, "id", "Defence", tid);
                        sa = (Integer) db.get(Tables.GTS_T, "id", "SpAttack", tid);
                        sd = (Integer) db.get(Tables.GTS_T, "id", "SpDefence", tid);
                        s = (Integer) db.get(Tables.GTS_T, "id", "Speed", tid);
                        hpE = (Integer) db.get(Tables.GTS_T, "id", "HPEV", tid);
                        aE = (Integer) db.get(Tables.GTS_T, "id", "AttackEV", tid);
                        dE = (Integer) db.get(Tables.GTS_T, "id", "DefenceEV", tid);
                        saE = (Integer) db.get(Tables.GTS_T, "id", "SpAttackEV", tid);
                        sdE = (Integer) db.get(Tables.GTS_T, "id", "SpDefenceEV", tid);
                        sE = (Integer) db.get(Tables.GTS_T, "id", "SpeedEV", tid);
                        hpI = (Integer) db.get(Tables.GTS_T, "id", "HPIV", tid);
                        aI = (Integer) db.get(Tables.GTS_T, "id", "AttackIV", tid);
                        dI = (Integer) db.get(Tables.GTS_T, "id", "DefenceIV", tid);
                        saI = (Integer) db.get(Tables.GTS_T, "id", "SpAttackIV", tid);
                        sdI = (Integer) db.get(Tables.GTS_T, "id", "SpDefenceIV", tid);
                        sI = (Integer) db.get(Tables.GTS_T, "id", "SpeedIV", tid);
                        p.writeToNBT(nbt);
                        nbt.setString("originalTrainer", OT);
                        nbt.setInteger("Level", lvl);
                        nbt.setInteger("StatsAttack", a);
                        nbt.setInteger("StatsDefence", d);
                        nbt.setInteger("StatsHP", hp);
                        nbt.setInteger("StatsSpecialAttack", sa);
                        nbt.setInteger("StatsSpecialDefence", sd);
                        nbt.setInteger("StatsSpeed", s);
                        nbt.setInteger("EVHP", hpE);
                        nbt.setInteger("EVAttack", aE);
                        nbt.setInteger("EVDefence", dE);
                        nbt.setInteger("EVSpecialAttack", saE);
                        nbt.setInteger("EVSpecialDefence", sdE);
                        nbt.setInteger("EVSpeed", sE);
                        nbt.setInteger("IVHP", hpI);
                        nbt.setInteger("IVAttack", aI);
                        nbt.setInteger("IVDefence", dI);
                        nbt.setInteger("IVSpAtt", saI);
                        nbt.setInteger("IVSpDef", sdI);
                        nbt.setInteger("IVSpeed", sI);
                        nbt.setInteger("pixelmontid1", entityPlayer.getDisplayName().hashCode());
                        nbt.setInteger("pixelmontid2", -1);
                        nbt.setInteger("IsShiny", (shine.equals("true") ? 1 : 0));

                        p.readEntityFromNBT(nbt);

                        PlayerStorage s;
                        try {
                            s = PixelmonStorage.PokeballManager.getPlayerStorage(entityPlayer);
                            if (s.hasSpace()) {
	                            int pos = s.getNextOpen();
	                            s.changePokemonAndAssignID(pos, nbt);
                            } else {
                            	PlayerComputerStorage c = PixelmonStorage.ComputerManager.getPlayerStorage(entityPlayer);
                            	c.addToComputer(p);
                            }
	                        db.remove(Tables.GTS_T, "id", tid);
                        } catch (PlayerNotLoadedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utilities.sendMessageToCommandSender(entityPlayer, EnumChatFormatting.RED + "There is no trade by that ID! (/gts list)");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public EntityPixelmon createPoke(EntityPlayerMP sep, String sname, World sworld) throws SQLException {
        final String name = sname;
        final World world = sworld;
        final EntityPlayerMP ep = sep;
        final EntityPixelmon p = (EntityPixelmon) PixelmonEntityList.createEntityByName(name, world);

        // Same with this one
        new Thread(new Runnable() {
            public void run() {
                try {
                    db.connect();
                    String player = ep.getDisplayName();
                    if (db.contains(Tables.STOR, "PName", player)) {
                        NBTTagCompound nbt = new NBTTagCompound();
                        nature = (String) db.get(Tables.STOR, "PName", "Nature", player);
                        growth = (String) db.get(Tables.STOR, "PName", "Growth", player);
                        gender = (String) db.get(Tables.STOR, "PName", "Gender", player);
                        shine = (String) db.get(Tables.STOR, "PName", "Shiny", player);
                        m1 = (String) db.get(Tables.STOR, "PName", "Move1", player);
                        m2 = (String) db.get(Tables.STOR, "PName", "Move2", player);
                        m3 = (String) db.get(Tables.STOR, "PName", "Move3", player);
                        m4 = (String) db.get(Tables.STOR, "PName", "Move4", player);
                        ball = (String) db.get(Tables.STOR, "PName", "Ball", player);
                        if (m2 != null && m3 != null && m4 != null) {
                            p.loadMoveset(m1, m2, m3, m4);
                        } else if (m2 != null && m3 != null) {
                            p.loadMoveset(m1, m2, m3);
                        } else if (m2 != null) {
                            p.loadMoveset(m1, m2);
                        } else {
                            p.loadMoveset(m1);
                        }
                        p.setNature(EnumNature.natureFromString(nature));
                        p.setGrowth(EnumGrowth.growthFromString(growth));
                        p.gender = Gender.valueOf(gender);
                        p.caughtBall = EnumPokeballs.valueOf(ball);
                        OT = (String) db.get(Tables.STOR, "PName", "OTrainer", player);
                        lvl = (Integer) db.get(Tables.STOR, "PName", "Level", player);
                        hp = (Integer) db.get(Tables.STOR, "PName", "HP", player);
                        a = (Integer) db.get(Tables.STOR, "PName", "Attack", player);
                        d = (Integer) db.get(Tables.STOR, "PName", "Defence", player);
                        sa = (Integer) db.get(Tables.STOR, "PName", "SpAttack", player);
                        sd = (Integer) db.get(Tables.STOR, "PName", "SpDefence", player);
                        s = (Integer) db.get(Tables.STOR, "PName", "Speed", player);
                        hpE = (Integer) db.get(Tables.STOR, "PName", "HPEV", player);
                        aE = (Integer) db.get(Tables.STOR, "PName", "AttackEV", player);
                        dE = (Integer) db.get(Tables.STOR, "PName", "DefenceEV", player);
                        saE = (Integer) db.get(Tables.STOR, "PName", "SpAttackEV", player);
                        sdE = (Integer) db.get(Tables.STOR, "PName", "SpDefenceEV", player);
                        sE = (Integer) db.get(Tables.STOR, "PName", "SpeedEV", player);
                        hpI = (Integer) db.get(Tables.STOR, "PName", "HPIV", player);
                        aI = (Integer) db.get(Tables.STOR, "PName", "AttackIV", player);
                        dI = (Integer) db.get(Tables.STOR, "PName", "DefenceIV", player);
                        saI = (Integer) db.get(Tables.STOR, "PName", "SpAttackIV", player);
                        sdI = (Integer) db.get(Tables.STOR, "PName", "SpDefenceIV", player);
                        sI = (Integer) db.get(Tables.STOR, "PName", "SpeedIV", player);
                        p.writeToNBT(nbt);
                        nbt.setString("originalTrainer", OT);
                        nbt.setInteger("Level", lvl);
                        nbt.setInteger("StatsAttack", a);
                        nbt.setInteger("StatsDefence", d);
                        nbt.setInteger("StatsHP", hp);
                        nbt.setInteger("StatsSpecialAttack", sa);
                        nbt.setInteger("StatsSpecialDefence", sd);
                        nbt.setInteger("StatsSpeed", s);
                        nbt.setInteger("EVHP", hpE);
                        nbt.setInteger("EVAttack", aE);
                        nbt.setInteger("EVDefence", dE);
                        nbt.setInteger("EVSpecialAttack", saE);
                        nbt.setInteger("EVSpecialDefence", sdE);
                        nbt.setInteger("EVSpeed", sE);
                        nbt.setInteger("IVHP", hpI);
                        nbt.setInteger("IVAttack", aI);
                        nbt.setInteger("IVDefence", dI);
                        nbt.setInteger("IVSpAtt", saI);
                        nbt.setInteger("IVSpDef", sdI);
                        nbt.setInteger("IVSpeed", sI);
                        nbt.setInteger("IsShiny", (shine.equals("true") ? 1 : 0));
                        nbt.setInteger("pixelmonID1", ep.getDisplayName().hashCode());
                        nbt.setInteger("pixelmonID2", -1);

                        p.readEntityFromNBT(nbt);
                    } else {
                        Utilities.sendMessageToCommandSender(ep, EnumChatFormatting.RED + "There is no trade by that ID! (/gts list)");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return p;
    }

    public void offAddToDB(String player, EntityPixelmon poke) throws SQLException {
        db.connect();
        if (db.contains(Tables.GTS_T, "SellerName", player)) {
            name = EnumPokemon.get(poke.getName()).name;
            pName = player;
            OT = poke.originalTrainer;
            lvl = poke.getLvl().getLevel();
            ball = poke.caughtBall.toString();
            nature = poke.getNature().toString();
            gender = poke.gender.toString();
            growth = poke.getGrowth().toString();
            Moveset moves = poke.getMoveset();
            m1 = moves.get(0).baseAttack.getLocalizedName();
            if (moves.attacks[1] != null)
                m2 = moves.get(1).baseAttack.getLocalizedName();
            else
                m2 = null;
            if (moves.attacks[2] != null)
                m3 = moves.get(2).baseAttack.getLocalizedName();
            else
                m3 = null;
            if (moves.attacks[3] != null)
                m4 = moves.get(3).baseAttack.getLocalizedName();
            else
                m4 = null;
            hp = Integer.valueOf(poke.stats.HP);
            a = Integer.valueOf(poke.stats.Attack);
            d = Integer.valueOf(poke.stats.Defence);
            sa = Integer.valueOf(poke.stats.SpecialAttack);
            sd = Integer.valueOf(poke.stats.SpecialDefence);
            s = Integer.valueOf(poke.stats.Speed);
            hpE = Integer.valueOf(poke.stats.EVs.HP);
            aE = Integer.valueOf(poke.stats.EVs.Attack);
            dE = Integer.valueOf(poke.stats.EVs.Defence);
            saE = Integer.valueOf(poke.stats.EVs.SpecialAttack);
            sdE = Integer.valueOf(poke.stats.EVs.SpecialDefence);
            sE = Integer.valueOf(poke.stats.EVs.Speed);
            hpI = Integer.valueOf(poke.stats.IVs.HP);
            aI = Integer.valueOf(poke.stats.IVs.Attack);
            dI = Integer.valueOf(poke.stats.IVs.Defence);
            saI = Integer.valueOf(poke.stats.IVs.SpAtt);
            sdI = Integer.valueOf(poke.stats.IVs.SpDef);
            sI = Integer.valueOf(poke.stats.IVs.Speed);
            shine = (poke.getIsShiny() ? "true" : "false");

            // Same here
            new Thread(new Runnable() {
                public void run() {
                    try {
                        db.connect();
                        db.set(Tables.STOR, null, pName, name, OT, lvl, ball, nature, gender, growth, m1, m2, m3, m4, hp, a, d, sa, sd, s, hpE, aE, dE, saE, sdE, sE, hpI, aI, dI, saI, sdI, sI, shine);
                        db.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public void addToDB(EntityPlayerMP player, EntityPixelmon poke, EntityPixelmon wPoke, boolean sh) throws SQLException {
        name = EnumPokemon.get(poke.getName()).name;
        wanted = EnumPokemon.get(wPoke.getName()).name;
        seller = UUIDHelper.getOfflineUUID(player.getDisplayName());
        sellerName = player.getDisplayName();
        OT = poke.originalTrainer;
        lvl = poke.getLvl().getLevel();
        ball = poke.caughtBall.toString();
        nature = poke.getNature().toString();
        gender = poke.gender.toString();
        growth = poke.getGrowth().toString();
        Moveset moves = poke.getMoveset();
        m1 = moves.get(0).baseAttack.getLocalizedName();
        if (moves.attacks[1] != null)
            m2 = moves.get(1).baseAttack.getLocalizedName();
        else
            m2 = null;
        if (moves.attacks[2] != null)
            m3 = moves.get(2).baseAttack.getLocalizedName();
        else
            m3 = null;
        if (moves.attacks[3] != null)
            m4 = moves.get(3).baseAttack.getLocalizedName();
        else
            m4 = null;
        hp = Integer.valueOf(poke.stats.HP);
        a = Integer.valueOf(poke.stats.Attack);
        d = Integer.valueOf(poke.stats.Defence);
        sa = Integer.valueOf(poke.stats.SpecialAttack);
        sd = Integer.valueOf(poke.stats.SpecialDefence);
        s = Integer.valueOf(poke.stats.Speed);
        hpE = Integer.valueOf(poke.stats.EVs.HP);
        aE = Integer.valueOf(poke.stats.EVs.Attack);
        dE = Integer.valueOf(poke.stats.EVs.Defence);
        saE = Integer.valueOf(poke.stats.EVs.SpecialAttack);
        sdE = Integer.valueOf(poke.stats.EVs.SpecialDefence);
        sE = Integer.valueOf(poke.stats.EVs.Speed);
        hpI = Integer.valueOf(poke.stats.IVs.HP);
        aI = Integer.valueOf(poke.stats.IVs.Attack);
        dI = Integer.valueOf(poke.stats.IVs.Defence);
        saI = Integer.valueOf(poke.stats.IVs.SpAtt);
        sdI = Integer.valueOf(poke.stats.IVs.SpDef);
        sI = Integer.valueOf(poke.stats.IVs.Speed);
        shine = (poke.getIsShiny() ? "true" : "false");
        ws = (sh ? "true" : "false");

        // Here too..
        new Thread(new Runnable() {
            public void run() {
                try {
                    db.connect();
                    db.set(Tables.GTS_T, null, seller, sellerName, name, OT, lvl, ball, nature, gender, growth, m1, m2, m3, m4, hp, a, d, sa, sd, s, hpE, aE, dE, saE, sdE, sE, hpI, aI, dI, saI, sdI, sI, shine, wanted, ws);
                    db.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    String name, OT, ball, nature, growth, gender, m1, m2, m3, m4, wanted, shine, ws, sellerName, pName;
    UUID seller;
    int lvl, hp, a, d, sa, sd, s, hpE, aE, dE, saE, sdE, sE, hpI, aI, dI, saI, sdI, sI;
    boolean sh;
}