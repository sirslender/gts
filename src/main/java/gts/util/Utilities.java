package gts.util;


import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;

public class Utilities {

    public static void sendServerwideMessage(String message) {
        ChatComponentTranslation chatcomponenttranslation = new ChatComponentTranslation(message, new Object[0]);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(chatcomponenttranslation);
    }

    public static void sendMessageToPlayer(EntityPlayerMP player, String message) {
        ChatComponentTranslation chatcomponenttranslation = new ChatComponentTranslation(message, new Object[0]);
        player.addChatMessage(chatcomponenttranslation);
    }

    public static void sendMessageToCommandSender(ICommandSender sender, String message) {
        try {
            EntityPlayerMP player = CommandBase.getPlayer(sender, sender.getCommandSenderName());
            sendMessageToPlayer(player, message);
        } catch (Exception e) {
            System.out.println(message);
        }
    }
}
