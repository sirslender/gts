package gts.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

public class PixelXPPermissions
{
    public static PixelXPPermissions Instance = new PixelXPPermissions();
    private Class<?> bukkit;
    private Method getPlayer;
    private Method hasPermission;

    public PixelXPPermissions()
    {
        try
        {
            this.bukkit = Class.forName("org.bukkit.Bukkit");
            this.getPlayer = this.bukkit.getMethod("getPlayer", new Class[] { String.class });
            this.hasPermission = Class.forName("org.bukkit.entity.Player").getMethod("hasPermission", new Class[] { String.class });
            System.out.println("Bukkit permissions enabled");
        } catch (ClassNotFoundException e) {
        }
        catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static boolean hasPermission(EntityPlayer player, String permission) {
        if (Instance.bukkit != null) {
            return Instance.bukkitPermission(player.getDisplayName(), permission);
        }
        return true;
    }

    private boolean bukkitPermission(String username, String permission) {
        try {
            Object player = this.getPlayer.invoke(null, new Object[] { username });
            return ((Boolean)this.hasPermission.invoke(player, new Object[] { permission })).booleanValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
