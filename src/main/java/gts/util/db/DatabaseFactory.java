package gts.util.db;

import gts.Base;

import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.common.config.Configuration;

public class DatabaseFactory {
	private final Base plugin;
	private final List<Converter> converters = new ArrayList<Converter>();

	public DatabaseFactory(Base plugin) {
		this.plugin = plugin;
	}

	/**
	 * Register a new converter.
	 * 
	 * @param converter Converter to register
	 */
	public void registerConverter(Converter converter) {
		converters.add(converter);
	}

	/**
	 * Construct a database
	 * 
	 * @param builder Database configuration
	 * @return New database
	 */
	public Database getDatabase(DatabaseConfigBuilder builder) {
		return new Database(this, builder);
	}

	/**
	 * Generates a default config section with the name of MySQL (defaults to sqlite)
	 */
	public void generateConfigSection() {
		Configuration config = DatabaseConfigBuilder.config;
		config.load();
		config.getString("StorageType", config.CATEGORY_GENERAL, "MYSQL", "");
		config.getString("host", config.CATEGORY_GENERAL, "127.0.0.1", "");
		config.getString("port", config.CATEGORY_GENERAL, "3306", "");
		config.getString("user", config.CATEGORY_GENERAL, "root", "");
		config.getString("password", config.CATEGORY_GENERAL, "INSERT PASSWORD", "");
		config.getString("database", config.CATEGORY_GENERAL, Base.MODNAME, "");
		config.save();
	}

	protected void doConversion(Database database) {
		for(Converter converter : converters) {
			converter.onDatabaseLoad(database);
		}
	}
}