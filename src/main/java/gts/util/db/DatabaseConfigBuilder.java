package gts.util.db;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class DatabaseConfigBuilder {
	private String driver;
	private String url;
	private String database;
	private String user;
	private String password;
	private String file;
	private String sql;

	/**
	 * Default constructor, no settings.
	 */
	public DatabaseConfigBuilder() {}

	/**
	 * Construct a database based on a config section with a driver and custom url.
	 * This is quite advanced.
	 * 
	 * @param section Configuration section.
	 */
	public DatabaseConfigBuilder(File configFile) {
		config = new Configuration(configFile);
		config.load();
		driver(config.get(Configuration.CATEGORY_GENERAL, "driver", "com.mysql.jdbc.Driver").getString()).url(config.get(Configuration.CATEGORY_GENERAL, "url", "127.0.0.1:3306").getString()).database(config.get(Configuration.CATEGORY_GENERAL, "database", "gts").getString()).user(config.get(Configuration.CATEGORY_GENERAL, "user", "root").getString()).password(config.get(Configuration.CATEGORY_GENERAL, "password", "password").getString());
		config.save();
	}

	/**
	 * Construct a database based on a config section with sqlite backup, drivers auto-generated.
	 * 
	 * @param section Configuration section.
	 * @param backup SQLIte file backup.
	 */
	public DatabaseConfigBuilder(File configFile, File backup) {
		config = new Configuration(configFile);
		try {
			config.load();
			if(config.get(Configuration.CATEGORY_GENERAL, "StorageType", "MYSQL").getString().equals("MYSQL")) {
				setStorage("MYSQL");
				String url = String.format("%s:%s", config.get(Configuration.CATEGORY_GENERAL, "host", "127.0.0.1").getString(), config.get(Configuration.CATEGORY_GENERAL, "port", "3306").getString());
				driver("com.mysql.jdbc.Driver").url(url).database(config.get(Configuration.CATEGORY_GENERAL, "database", "gts").getString()).user(config.get(Configuration.CATEGORY_GENERAL, "user", "root").getString()).password(config.get(Configuration.CATEGORY_GENERAL, "password", "password").getString());
			} else if (config.get(Configuration.CATEGORY_GENERAL, "StorageType", "MYSQL").getString().equals("SQL")) {
				setStorage("SQL");
				driver("org.sqlite.JDBC").sqlite(backup);
			} else {
				setStorage("MYSQL");
				String url = String.format("%s:%s", config.get(Configuration.CATEGORY_GENERAL, "host", "127.0.0.1").getString(), config.get(Configuration.CATEGORY_GENERAL, "port", "3306").getString());
				driver("com.mysql.jdbc.Driver").url(url).database(config.get(Configuration.CATEGORY_GENERAL, "database", "gts").getString()).user(config.get(Configuration.CATEGORY_GENERAL, "user", "root").getString()).password(config.get(Configuration.CATEGORY_GENERAL, "password", "password").getString());
			}
			config.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DatabaseConfigBuilder driver(String driver) {
		this.driver = driver;
		return this;
	}

	public DatabaseConfigBuilder url(String url) {
		this.url = url;
		return this;
	}

	public DatabaseConfigBuilder database(String database) {
		this.database = database;
		return this;
	}

	public DatabaseConfigBuilder user(String user) {
		this.user = user;
		return this;
	}

	public DatabaseConfigBuilder password(String password) {
		this.password = password;
		return this;
	}

	public DatabaseConfigBuilder sqlite(File file) {
		this.file = file.getPath();
		return this;
	}

	public String getFile() {
		return file;
	}

	public String getDriver() {
		return driver;
	}

	public String getUrl() {
		return url;
	}

	public String getDatabase() {
		return database;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}
	
	public String getStorage(){
		return sql;
	}
	
	public void setStorage(String set){
		sql = set;
	}
	

	public static Configuration config;
}