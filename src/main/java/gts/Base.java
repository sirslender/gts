package gts;

import gts.util.Utilities;
import gts.util.db.Database;
import gts.util.db.DatabaseConfigBuilder;
import gts.util.db.DatabaseFactory;

import java.io.File;
import java.sql.SQLException;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.Configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerComputerStorage;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.network.NetworkCheckHandler;
import cpw.mods.fml.relauncher.Side;

@Mod(modid=Base.MODID, name=Base.MODNAME, version=Base.VERSION, dependencies="required-after:pixelmon;", acceptableRemoteVersions="*")
public class Base {
	public static final String MODID = "GTS";
	public static final String MODNAME = "Global Trade System";
	public static final String VERSION = "1.0.1";
	
	@Instance("GTS")
	public static Base instance = new Base();
	public static final Logger logger = LogManager.getLogger(MODID);
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) throws SQLException{
		FMLCommonHandler.instance().bus().register(instance);
		DatabaseFactory factory = new DatabaseFactory(this);
		File section = event.getSuggestedConfigurationFile();
		File modDir = Pixelmon.modDirectory;
		File sqliteFile = new File(modDir + "/database/", "database.db");
		DatabaseConfigBuilder config = new DatabaseConfigBuilder(section, sqliteFile);
		factory.generateConfigSection();
		Database database = factory.getDatabase(config);
		database.connect();
		db = database;
		db.registerTable(Tables.GTS_T);
		db.registerTable(Tables.STOR);
		storage = config.getStorage();
		db.close();
	}
	
	@Mod.EventHandler
	public void Init(FMLInitializationEvent event) {}
	
	@Mod.EventHandler
	public void onServerStart(FMLServerStartingEvent event) {
		event.registerServerCommand(new GTSCommand());	
	}
	
	@SubscribeEvent
	public void onLogin(PlayerLoggedInEvent e) throws SQLException{
		db.connect();
		if (db.contains(Tables.STOR, "PName", e.player.getDisplayName())) {
			String pname = e.player.getDisplayName();
			World pworld = e.player.worldObj;
			GTSCommand gt = new GTSCommand();
			String pokeName = (String) db.get(Tables.STOR, "PName", "pokemon", pname);
			EntityPlayerMP eplayer = MinecraftServer.getServer().getConfigurationManager().func_152612_a(pname);
			EntityPixelmon pix = gt.createPoke(eplayer, pokeName, pworld);
			PlayerStorage s = null;
			try {
				s = PixelmonStorage.PokeballManager.getPlayerStorage((EntityPlayerMP) e.player);
			} catch (PlayerNotLoadedException e1) {
				e1.printStackTrace();
			}
			NBTTagCompound nbt = pix.getEntityData();
			if (s.hasSpace()) {
				int slot = s.getNextOpen();
				s.changePokemonAndAssignID(slot, nbt);
			} else {
				PlayerComputerStorage c = PixelmonStorage.ComputerManager.getPlayerStorage(eplayer);
				c.addToComputer(pix);
			}
			db.remove(Tables.STOR, "PName", pname);
			Utilities.sendMessageToPlayer((EntityPlayerMP) e.player, EnumChatFormatting.GREEN + "[" + EnumChatFormatting.DARK_AQUA + "GTS" + EnumChatFormatting.GREEN + "] You received a pokemon from the GTS!");
		}
	}
	
	public Database db;
	public String storage;

}
